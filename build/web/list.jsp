<%-- 
    Document   : list
    Created on : Jan 8, 2020, 9:16:36 AM
    Author     : xuanvinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <th>Full Name</th>
            <th>Birthday</th>
            <th>Address</th>
            <th>Position</th>
            <th>Department</th>
            <c:forEach var="emp" items="${getAllEmployee}">
            <tr>
                <td>${emp.fullname}</td>
                <td>${emp.birthday}</td>
                <td>${emp.address}</td>
                <td>${emp.position}</td>
                <td>${emp.department}</td>
            </tr>
            </c:forEach>
        </table>
    </body>
</html>
