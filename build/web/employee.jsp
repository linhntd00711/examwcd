<%-- 
    Document   : employee
    Created on : Jan 8, 2020, 9:15:44 AM
    Author     : xuanvinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>       
        
        <form action="EmployeeServlet" method="POST">
            <table>
                <tr>
                    <td>FullName:</td>
                    <td><input type="text" name="fullname" required/></td>
                </tr>
                 <tr>
                    <td>Birthday:</td>
                    <td><input type="text" name="birthday" required=""></td>
                </tr>
                 <tr>
                    <td>Address:</td>
                    <td><input type="text" name="address" required=""></td>
                </tr>
                 <tr>
                    <td>Position:</td>
                    <td><input type="text" name="position" required=""></td>
                </tr>
                 <tr>
                    <td>Department:</td>
                    <td><input type="text" name="department" required></td>
                </tr>
                
                <tr>
                    <td colspan="2">
                    <input type="submit" value="Submit"/>
                    <input type="submit" value="Reset"/></td>
                </tr>
                
            </table>
        </form>
    </body>
</html>
