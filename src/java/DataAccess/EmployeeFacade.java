/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import Entity.Employee;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author xuanvinh
 */
@Stateless
public class EmployeeFacade extends AbstractFacade<Employee> implements EmployeeFacadeLocal {

    @PersistenceContext(unitName = "WCD1ExamPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeFacade() {
        super(Employee.class);
    }

    @Override
    public List<Employee> listEmployee() {
        Query query = em.createNativeQuery("select * from employee", Employee.class);
        
        List<Employee> list = (List<Employee>) query.getResultList();
        
        return list;
    }

    @Override
    public boolean addEmployee(String fullname, Date birthday, String address, String position, String department) {
        Query query = em.createNativeQuery("insert into employee(fullname, birthday, address, position, department) values(?, ?, ?, ?, ?)");
        
        query.setParameter(1, fullname);
        query.setParameter(2, birthday);
        query.setParameter(3, address);
        query.setParameter(4, position);
        query.setParameter(5, department);
        
        return query.executeUpdate() != 0;
    }
    
}
